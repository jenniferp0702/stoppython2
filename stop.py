from graphics import *

def main():

    ancho=200
    largo=300

    win=GraphWin("stop",ancho,largo)

    message = Text(Point(100, 20), "Nombre")
    message.draw(win)
    textoNombre = Entry(Point(100, 50), 10)
    textoNombre.setFill("pink")
    textoNombre.draw(win)

    message = Text(Point(200, 20), "Ciudad")
    message.draw(win)
    textoCiudad = Entry(Point(200, 50), 10)
    textoCiudad.setFill("pink")
    textoCiudad.draw(win)

    message = Text(Point(400, 200), "Cosa")
    message.draw(win)
    textoCosa = Entry(Point(400, 50), 10)
    textoCosa.setFill("pink")
    textoCosa.draw(win)

    message = Text(Point(300, 20), "Animal")
    message.draw(win)
    textoAnimal = Entry(Point(300, 50), 10)
    textoAnimal.setFill("pink")
    textoAnimal.draw(win)

    botonParar=Rectangle(Point((ancho/2)-50,150),Point((ancho/2)+50,200))
    botonParar.draw(win)


    leerArchivo("nombre.txt")
    win.getMouse() # Pause to view result
    win.close()    # Close window when done

def leerArchivo(nombre):
    f = open(nombre, 'r')
    mensaje = f.readline()
    print(mensaje)
    f.close()

main()